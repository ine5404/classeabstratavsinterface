/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ine5404;

/**
 *
 * @author martin.vigil
 */
public class Circulo implements Figura2d {
    protected double raio;

    public double getRaio() {
        return raio;
    }
    
    public Circulo(double raio){
        this.raio = raio;
    }
    
    public double getArea(){
        return Math.PI * this.raio * this.raio;
    }
    
  
}
