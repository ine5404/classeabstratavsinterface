/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ine5404;

/**
 *
 * @author martin.vigil
 */
public class Bloco extends Retangulo implements Figura3d {
    protected double profundidade;
    
    public Bloco(double base, double altura, double profundidade){
        super(base, altura);
        this.profundidade = profundidade;                
    }
    
    @Override
    public double getArea() {
        return 2 * super.getArea() +
                + 2 * this.getAltura() * this.profundidade
                + 2 * this.getBase() * this.profundidade;
    }
    
    @Override
    public double getVolume() {
        return super.getArea() * this.profundidade;
    }
    
}
