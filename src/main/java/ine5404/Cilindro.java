/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ine5404;

/**
 *
 * @author martin.vigil
 */
public class Cilindro extends Circulo implements Figura3d {
    protected double altura;
    
    public Cilindro(double raio, double altura){
        super(raio);
        this.altura = altura;
    }
    
    @Override
    public double getVolume() {
        return super.getArea() * this.altura;
    }
    
    @Override 
    public double getArea(){
        return 2 * super.getArea() 
                + this.altura * 2 * Math.PI * this.getRaio();
    }
}
