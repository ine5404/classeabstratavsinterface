/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ine5404;

/**
 *
 * @author martin.vigil
 */
public class Main {
    public static void main(String args[]){
        Figura2d[] figuras2d = new Figura2d[4];
        
        figuras2d[0] = new Circulo(10);
        figuras2d[1] = new Cilindro(10,10);
        figuras2d[2] = new Retangulo(1,1);
        figuras2d[3] = new Bloco(1,1,1);
        
        for(Figura2d figura : figuras2d){
            System.out.println(figura.getClass());
        }
        
    }
}
